const items = require('../items.js');
const find = require('../find.js');
const varToCheck = 2;
const cb = (elements) => {
    return elements === varToCheck ? true : false;
}
const result = find(items, cb);
const expectedResult = 2;
if (result === expectedResult) {
    console.log("Result Matches and the Result is " + result);
} else {
    console.log("Results are different");
}