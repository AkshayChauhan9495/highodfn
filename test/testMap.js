const items = require('../items.js');
const map = require('../map.js');
let cb = (num) => num * num;
const result = map(items, cb);
const expectedResult = [1, 4, 9, 16, 25, 25];
if (JSON.stringify(expectedResult) === JSON.stringify(result)) {
    console.log(result);
}
else {
    console.log("Results are different");
}