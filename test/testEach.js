const items = require('../items.js');
const each = require('../each.js');
let cb = (value, index) => console.log("Index of " + value + " is " + index);
const result = each(items, cb);
if (result === undefined) {
    console.log("Results are same.");
}
else {
    console.log("Results are different.");
}