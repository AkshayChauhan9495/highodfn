const items = require('../items.js');
const filter = require('../filter.js');
const number = 2;
let cb = (num) => num > number;
const expectedResult = [3, 4, 5, 5];
const actualResult = filter(items, cb);
if (JSON.stringify(expectedResult) === JSON.stringify(actualResult)) {
    console.log(actualResult);
}
else {
    console.log("Results are different");
}