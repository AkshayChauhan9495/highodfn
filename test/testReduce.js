const items = require('../items.js');
const reduce = require('../reduce.js');

const cb = (startingValue, current) => startingValue + current;

const sum = reduce(items, cb);
const sum2 = reduce(items, cb, 5);
const expectedResult = 20;
const expectedResult2 = 25;

if (expectedResult === sum && expectedResult2 === sum2) {
    console.log("Result without initial value is " + sum);
    console.log("Result with initial value is " + sum2);
} else {
    console.log("Results are different");
}