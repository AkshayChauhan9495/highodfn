module.exports = function find(elements, cb) {
    let result;
    let isFound;
    for (let index = 0; index < elements.length; index++) {
        isFound = cb(elements[index], index, elements)
        if (isFound) {
            return elements[index];

        }
    }
    if (!isFound) {
        return undefined;
    }
}
