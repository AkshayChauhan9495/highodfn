module.exports = function filter(elements, cb) {
  let newArr = [];
  for (let index = 0; index < elements.length; index++) {
    result = cb(elements[index])
    if (result) {
      newArr.push(elements[index]);
    }
  }
  return newArr;
}
