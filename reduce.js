module.exports = function reduce(elements, cb, startingValue) {

  let accumulator = startingValue === undefined ? elements[0] : startingValue;
  let i = startingValue === undefined ? 1 : 0;

  for (let index = i; index < elements.length; index++) {
    accumulator = cb(accumulator, elements[index], index, elements);

  }

  return accumulator;
  
}
