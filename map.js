module.exports = function map(elements, cb) {
  const mapArr = [];
  for (let index = 0; index < elements.length; index++) {
    const result = cb(elements[index], index, elements);
    mapArr.push(result);
  }
  return mapArr;
}
